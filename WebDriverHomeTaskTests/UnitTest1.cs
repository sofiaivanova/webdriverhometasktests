using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace WebDriverHomeTaskTests
{
    public class Tests
    {
        private IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.google.com/");
        }

        [Test]
        public void Test1()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            IWebElement search_input = driver.FindElement(By.XPath("//input[@class='gLFyf gsfi']"));

            search_input.SendKeys("������");
            IWebElement search_button = driver.FindElement(By.XPath("//input[@type='submit']"));
            search_button.Click();

            IWebElement tag_image = driver.FindElement(By.XPath("//div/a[@data-hveid='CAIQAw']"));
            tag_image.Click();

            Assert.That(driver.FindElement(By.CssSelector("img.rg_i")).Displayed, Is.True);
            
        }
        [TearDown]

        public void Close_Browser()

        {
            driver.Quit();
        }
    }
}